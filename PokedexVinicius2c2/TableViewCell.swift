//
//  TableViewCell.swift
//  PokedexVinicius2c2
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var tipo1: UILabel!
    @IBOutlet weak var tipo2: UILabel!
    @IBOutlet weak var imagem: UIImageView!
    
}
