//
//  ListaViewController.swift
//  PokedexVinicius2c2
//
//  Created by COTEMIG on 04/08/22.
//

import UIKit

struct pokemon{
    var nome:String
    var tipo1:String
    var tipo2: String
    var imagem: String
    var cor: String
    
}

class ListaViewController: UIViewController,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var pokemons:[pokemon] = [
        pokemon(nome: "bulbassauro", tipo1: "grama", tipo2: "veneno", imagem: "image (3)", cor: "verde"),
        pokemon(nome: "Ivyssauro", tipo1: "grama", tipo2: "veneno", imagem: "ivysaur_png_722324 (1)", cor: "verde"),
        pokemon(nome: "Venussauro", tipo1: "grama", tipo2: "veneno", imagem: "venusaur_png_1459038 (1)", cor: "verde"),
        pokemon(nome: "Charmander", tipo1: "fogo", tipo2: "", imagem: "image (2)", cor: "vermelho"),
        pokemon(nome: "Charmeleon", tipo1: "fogo", tipo2: "", imagem: "005", cor: "vermelho"),

        
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "celula") as? TableViewCell else{ return UITableViewCell() }
        let pokemon = pokemons[indexPath.row]
        cell.nome.text = pokemon.nome
        cell.tipo1.text = pokemon.tipo1
        cell.tipo2.text = pokemon.tipo2
        cell.imagem.image = UIImage(named: pokemon.imagem)
        cell.contentView.backgroundColor = UIColor(named: pokemon.cor)
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
